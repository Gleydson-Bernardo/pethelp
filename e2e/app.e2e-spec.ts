import { PetHelpPage } from './app.po';

describe('pet-help App', function() {
  let page: PetHelpPage;

  beforeEach(() => {
    page = new PetHelpPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
